//
//  PalyGameViewController.m
//  YouWin31
//
//  Created by parkyoseop on 2016. 2. 17..
//  Copyright © 2016년 yoseop2da. All rights reserved.
//

#import "PlayGameViewController.h"

@import GoogleMobileAds;

typedef NS_ENUM(NSInteger, VsType) {
    VsTypeWithComputer,
    VsTypeWithUser
};

#define DEFAULT_COLOR   [UIColor colorWithRed:255/255.f green:226/255.f blue:255/255.f alpha:1.f]
#define USER_COLOR      [UIColor colorWithRed:242/255.f green:60/255.f blue:149/255.f alpha:1.f]
#define COMPUTER_COLOR  [UIColor colorWithRed:0/255.f green:94/255.f blue:170/255.f alpha:1.f]

@interface PlayGameViewController ()
{
    IBOutletCollection(UIButton) NSArray *numberButtons;
    IBOutlet UIView *blockView;
    IBOutlet UIImageView *loadingImageView;
    IBOutlet UIView *startView;
    IBOutlet UILabel *titleLabel;
    
    BOOL _isFirstUserTurn;
    int _lastNumber;

    VsType _vsType;
    
    // Loading
    int _animationIndex;
    NSTimer *_loadTimer;
    
    // Start View
    IBOutlet UIView *selectView;
    
    // Game Over View
    IBOutlet UIView *gameOverView;
    IBOutlet UILabel *gameOverLabel;
    
    // Banner
    IBOutlet GADBannerView *bannerView;
}
@end

@implementation PlayGameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
    [self addAdMob];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initialize
{
    _isFirstUserTurn = NO;
    _lastNumber = 0;
    _animationIndex = 0;
    _vsType = VsTypeWithComputer;
    selectView.alpha = 0.f;
    gameOverView.alpha = 0.f;
    [self hideBlockView];
    
    for (UIButton *nbutton in numberButtons) {
        nbutton.backgroundColor = DEFAULT_COLOR;
        nbutton.userInteractionEnabled = YES;
    }
}

#pragma mark -

- (IBAction)homeButtonTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)numberButtonTouched:(UIButton *)button
{
    int selectednumber = [button.titleLabel.text intValue];
    
    if (selectednumber - _lastNumber > 3) {
        // 해당 숫자는 선택할 수 없습니다.
        NSLog(@"해당 숫자는 선택할 수 없습니다.");
        [[[UIAlertView alloc] initWithTitle:@"해당 숫자는 선택할 수 없습니다." message:nil delegate:nil cancelButtonTitle:@"알겠음" otherButtonTitles:nil, nil] show];
        return;
    }
    
    [self drawUserButtonsWithSelectedNumberArray:[self selectedNumbersFromEndNumber:selectednumber] isUserTern: _isFirstUserTurn];
    
    if (_vsType == VsTypeWithComputer) {
        [self showBlockView];
        [self performSelector:@selector(computerTime) withObject:nil afterDelay:2.0f];
    }else{
        _isFirstUserTurn = !_isFirstUserTurn;
    }
}


#pragma mark -
- (void)drawUserButtonsWithSelectedNumberArray:(NSArray *)selectedNumberArray isUserTern:(BOOL)isUserTern
{
    for (UIButton *nbutton in numberButtons) {
        for (NSNumber *num in selectedNumberArray) {
            if (nbutton.tag == num.intValue) {
                NSLog(@"버튼 배경 변경 : %d", num.intValue);
                nbutton.backgroundColor = isUserTern? USER_COLOR : COMPUTER_COLOR;
                nbutton.userInteractionEnabled = NO;
                _lastNumber = (int)nbutton.tag;
                if (_lastNumber == 30) {
                    if (_vsType == VsTypeWithComputer) {
                        gameOverLabel.text = (isUserTern ? @"오!! 대단해~": @"날 이길 수 없어 ㅋㅋㅋㅋ");
                    }else{
                        gameOverLabel.text = @"게임 종료";
                    }
                    
                    [self showGameOverView];
                }
            }
        }
    }
}

- (void)computerTime
{
    int start = _lastNumber;
    int end;
    if (_lastNumber < 6) {
        if (_lastNumber < 3) {
            end = start+1;
        }else{
            end = 6;
        }
    }else{
        int mod = (_lastNumber-6)%4;
        switch (mod) {
            case 0:
                end = _lastNumber+1;
                break;
            case 1:
                end = _lastNumber+3;
                break;
            case 2:
                end = _lastNumber+2;
                break;
            case 3:
                end = _lastNumber+1;
                break;
            default:
                break;
        }
    }
    
    [self drawUserButtonsWithSelectedNumberArray:[self selectedNumbersFromEndNumber:end] isUserTern:NO];
}

- (NSArray *)selectedNumbersFromEndNumber:(int)end
{
    int start = _lastNumber+1;
    NSMutableArray *arr = [NSMutableArray array];
    while (start <= end) {
        [arr addObject:@(start)];
        start++;
    }
    NSLog(@"선택한 Arr : %@", arr);
    return arr;
}


#pragma mark - BlockView

- (void)showBlockView
{
    [self endAnimations];
    [self startAnimations];
    
    blockView.hidden = NO;
    [self performSelector:@selector(hideBlockView) withObject:nil afterDelay:2.0f];
}

- (void)hideBlockView
{
    [self endAnimations];
    blockView.hidden = YES;
}

- (void)startAnimations
{
    _loadTimer = [NSTimer scheduledTimerWithTimeInterval:0.15 target:self selector:@selector(changeLoadingImages) userInfo:nil repeats:YES];
}

- (void)endAnimations
{
    if ([_loadTimer isValid]) {
        [_loadTimer invalidate];
        _loadTimer = nil;
    }
}

- (void)changeLoadingImages
{
    if (_animationIndex > 5) {
        _animationIndex = 0;
    }
    loadingImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%03d",++_animationIndex]];
}

#pragma mark - StartView

- (void)showStartView
{
    [UIView animateWithDuration:0.8f animations:^{
        startView.alpha = 1.f;
    }];
}

- (void)hideStartView
{
    [UIView animateWithDuration:0.8f animations:^{
        startView.alpha = 0.f;
    }];
}

- (IBAction)vsModeButtonTouched:(UIButton *)sender
{
    if (sender.tag == 0) {
        // 사람 vs 컴퓨터
        _isFirstUserTurn = YES;
        _vsType = VsTypeWithComputer;
        titleLabel.text = @"사람 VS 컴퓨터";
        [UIView animateWithDuration:0.8f animations:^{
            selectView.alpha = 1.f;
        }];
    }else{
        // 사람 vs 사람
        _vsType = VsTypeWithUser;
        titleLabel.text = @"사람 vs 사람";
        [self hideStartView];
    }
}

- (IBAction)playFirstButtonTouched:(id)sender
{
    [self hideStartView];
}

- (IBAction)playerLaterButtonTouched:(id)sender
{
    [self hideStartView];
    
    [self showBlockView];
    [self performSelector:@selector(computerTime) withObject:nil afterDelay:1.0f];
}

#pragma mark - Game Over View

- (void)showGameOverView
{
    [UIView animateWithDuration:0.8f animations:^{
        gameOverView.alpha = 1.f;
    }];
}

- (void)hideGameOverView
{
    [UIView animateWithDuration:0.8f animations:^{
        gameOverView.alpha = 0.f;
    }];
}

- (IBAction)okButtonTouched:(id)sender
{
    [self initialize];
    [self hideGameOverView];
    [self showStartView];
}

#pragma mark - Google Ads

- (void)addAdMob
{
    bannerView.adUnitID = @"ca-app-pub-2783299302427084/7200570856";
    bannerView.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    [bannerView loadRequest:request];
}


@end
