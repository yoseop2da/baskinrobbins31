//
//  ViewController.m
//  YouWin31
//
//  Created by parkyoseop on 2016. 2. 16..
//  Copyright © 2016년 yoseop2da. All rights reserved.
//

#import "GuideMainViewController.h"
#import "GuideGameViewController.h"

@interface GuideMainViewController ()
{
    IBOutlet UIButton *firstButton;
    IBOutlet UIButton *secondButton;
}
@end

@implementation GuideMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions
- (IBAction)startButtonTouched:(UIButton *)sender
{
    GuideGameViewController *gameViewController = getViewControllerWithIdentifier(GuideGameViewController);
    if(sender.tag == 0){
        gameViewController.isMyTurn = YES;
    }else{
        gameViewController.isMyTurn = NO;
    }
    [self.navigationController pushViewController:gameViewController animated:YES];
}

- (IBAction)homeButtonTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
