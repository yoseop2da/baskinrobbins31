//
//  main.m
//  YouWin31
//
//  Created by parkyoseop on 2016. 2. 16..
//  Copyright © 2016년 yoseop2da. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
