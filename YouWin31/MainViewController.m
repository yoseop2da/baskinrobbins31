//
//  MainViewController.m
//  YouWin31
//
//  Created by parkyoseop on 2016. 2. 17..
//  Copyright © 2016년 yoseop2da. All rights reserved.
//

#import "MainViewController.h"
#import "IntroViewController.h"
#import "PlayGameViewController.h"
#import "GuideMainViewController.h"

@import GoogleMobileAds;

@interface MainViewController ()
{
    IBOutlet GADBannerView *bannerView;
}
@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addAdMob];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self showIntro];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)showIntro
{
    if (SharedAppDelegate.isIntroShown) { return; }
    IntroViewController *introViewController = getViewControllerWithIdentifier(IntroViewController);
    [self presentViewController:introViewController animated:NO completion:nil];
}

- (IBAction)vsModeButtonTouched:(id)sender
{
    
    PlayGameViewController *playGameViewController = getViewControllerWithIdentifier(PlayGameViewController);
    [self.navigationController pushViewController:playGameViewController animated:YES];
}

- (IBAction)guideButtonTouched:(id)sender
{
    GuideMainViewController *guideMainViewController = getViewControllerWithIdentifier(GuideMainViewController);
    [self.navigationController pushViewController:guideMainViewController animated:YES];
}

#pragma mark - Google Ads
- (void)addAdMob
{
    bannerView.adUnitID = @"ca-app-pub-2783299302427084/2909972056";
    bannerView.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    [bannerView loadRequest:request];
}

@end


