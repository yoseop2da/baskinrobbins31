//
//  IntroViewController.m
//  YouWin31
//
//  Created by parkyoseop on 2016. 2. 16..
//  Copyright © 2016년 yoseop2da. All rights reserved.
//

#import "IntroViewController.h"

@interface IntroViewController ()
{
    IBOutlet UILabel *numlabel;

    NSTimer *_timer;
    int _index;
}
@end

@implementation IntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(updateNumber) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateNumber
{
    if (_index >= 31) {
        [self performSelector:@selector(dismissIntro) withObject:nil afterDelay:1.f];
        return;
    }
    numlabel.text = [@(++_index) stringValue];
    CGFloat fontSize = numlabel.font.pointSize;
    fontSize += 7;
    numlabel.font = [UIFont fontWithName:numlabel.font.fontName size:fontSize];
}

- (void)dismissIntro
{
    SharedAppDelegate.isIntroShown = YES;
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
