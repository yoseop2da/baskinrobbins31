//
//  GameViewController.m
//  YouWin31
//
//  Created by parkyoseop on 2016. 2. 16..
//  Copyright © 2016년 yoseop2da. All rights reserved.
//

#import "GuideGameViewController.h"

@import GoogleMobileAds;

@interface GuideGameViewController ()
{
    IBOutlet UIView *startView;
    
    IBOutlet UILabel *guideLabel;
    
    IBOutlet UIView *buttonsView;
    IBOutlet UIButton *button1;
    IBOutlet UIButton *button2;
    IBOutlet UIButton *button3;

    IBOutlet UILabel *solutionLabel;
    
    IBOutlet UIButton *nextButton;
    IBOutlet UIView *gameOverView;
    IBOutlet UILabel *gameOverLabel;
    
    int _lastNumber;
    NSTimer *_timer;
    IBOutlet GADBannerView *bannerView;
}
@end

@implementation GuideGameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
    
    [self addAdMob];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initialize
{
    _lastNumber = 0;
    solutionLabel.hidden = YES;
    gameOverView.alpha = 0.f;


    nextButton.enabled = NO;
    [self refreshViews];
}

- (IBAction)numberButtonTouched:(UIButton *)button
{
    nextButton.enabled = YES;
    _lastNumber = [button.titleLabel.text intValue];
    
    [self unselectAllButtons];
    
    if (button.tag == 1) {
        button1.selected = YES;
    }else if (button.tag == 2) {
        button2.selected = YES;
    }else{
        button3.selected = YES;
    }
    
    if (_lastNumber == 30) {
        nextButton.enabled = NO;
        [self gameOver];
    }
}

- (void)unselectAllButtons
{
    button1.selected = NO;
    button2.selected = NO;
    button3.selected = NO;
}

- (void)refreshViews
{
    if (_isMyTurn) {
        if (_lastNumber == 0) {
            guideLabel.text = @"숫자를 선택 해주세요.";
            guideLabel.textColor = [UIColor whiteColor];
            [self updateButtons];
        }else{
            guideLabel.text = @"다음의 숫를 말하세요.";
            guideLabel.textColor = [UIColor whiteColor];
            [self updateSolutions];
        }
    }else{
        guideLabel.text = @"상대방의 마지막 숫자를 선택해주세요.";
        guideLabel.textColor = [UIColor whiteColor];
        [self updateButtons];
    }
}

- (void)gameOver
{
    [UIView animateWithDuration:0.5f animations:^{
        gameOverView.alpha = 1.f;
    }];
}

- (void)updateButtons
{
    if (_lastNumber == 30) {
        // 게임 종료
        [self gameOver];
    }else{
        if ([_timer isValid]) {
            [_timer invalidate];
            _timer = nil;
        }
        
        solutionLabel.hidden = YES;
        buttonsView.hidden = NO;
        nextButton.enabled = NO;
        
        [button1 setTitle:[@(_lastNumber+1) stringValue] forState:UIControlStateNormal];
        [button2 setTitle:[@(_lastNumber+2) stringValue] forState:UIControlStateNormal];
        [button3 setTitle:[@(_lastNumber+3) stringValue] forState:UIControlStateNormal];
    }
}

- (void)ggamBBak
{
    solutionLabel.hidden = !solutionLabel.hidden;
}

- (void)updateSolutions
{
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(ggamBBak) userInfo:nil repeats:YES];
    buttonsView.hidden = YES;
    solutionLabel.hidden = NO;
    int start = _lastNumber;
    int end;
    if (_lastNumber < 6) {
        if (_lastNumber < 3) {
            end = start+1;
        }else{
            end = 6;
        }
    }else{
        int mod = (_lastNumber-6)%4;
        switch (mod) {
            case 0:
                end = _lastNumber+1;
                break;
            case 1:
                end = _lastNumber+3;
                break;
            case 2:
                end = _lastNumber+2;
                break;
            case 3:
                end = _lastNumber+1;
                break;
            default:
                break;
        }
    }
    
    _lastNumber = end;
    
    solutionLabel.text = [self stringFromStart:start+1 end:end];
}

- (NSString *)stringFromStart:(int)start end:(int)end
{
    NSMutableArray *arr = [NSMutableArray array];
    while (start <= end) {
        [arr addObject:@(start)];
        start++;
    }
    return [arr componentsJoinedByString:@","];
}

- (IBAction)nextButtonTouched:(id)sender
{
    [self unselectAllButtons];
    _isMyTurn = !_isMyTurn;
    [self refreshViews];
}

- (IBAction)backButtonTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Google Ads

- (void)addAdMob
{
    bannerView.adUnitID = @"ca-app-pub-2783299302427084/7060970057";
    bannerView.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    [bannerView loadRequest:request];
}

@end
