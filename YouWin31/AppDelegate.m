//
//  AppDelegate.m
//  YouWin31
//
//  Created by parkyoseop on 2016. 2. 16..
//  Copyright © 2016년 yoseop2da. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [self initailMainStoryboard];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {}
- (void)applicationDidEnterBackground:(UIApplication *)application {}
- (void)applicationWillEnterForeground:(UIApplication *)application {}
- (void)applicationDidBecomeActive:(UIApplication *)application {}
- (void)applicationWillTerminate:(UIApplication *)application {}

- (void)initailMainStoryboard
{
    _mainStroyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}
@end
